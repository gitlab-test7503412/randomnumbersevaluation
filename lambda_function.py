import json
import urllib3
import boto3
from datetime import datetime

s3 = boto3.client('s3')
http = urllib3.PoolManager()

def get_random_numbers():
    # Function to fetch random numbers from the API
    url = "https://www.random.org/sequences/?min=1&max=100&format=plain&rnd=new&col=4"
    response = http.request('GET', url)
    
    # Extracting and formatting random numbers from the API response
    raw_data = response.data
    decoded_data = raw_data.decode()
    trimmed_data = decoded_data.strip()
    lines = trimmed_data.split('\n')
    
    # Initialize an empty list to store the final result
    numbers = []
    
    # Iterate through each line in the list of lines
    for line in lines:
        # Split each line into individual values using spaces as separators
        values_as_strings = line.split()
        
        # Convert each of these values from string to integer
        values_as_integers = [int(value) for value in values_as_strings]
        
        # Append the list of integers to the final result list
        numbers.append(values_as_integers)
    
    return numbers

def evaluate_peak_values(rows):
    # Function to calculate PEAK grade and suggestion
    peak_values = ["performance", "entrepreneurship", "authenticity", "kindness"]
    results = []

    for row in rows:
        result = {}
        for i, value in enumerate(row):
            grade = "PEAKer" if value >= 90 else "Not yet PEAKer"
            suggestion = "Keep up the great work, PEAKer!" if grade == "PEAKer" else "Work on improving in this area."

            result[peak_values[i]] = {
                "percentage": str(value),
                "grade": grade,
                "suggestion": suggestion
            }
        results.append(result)

    return results

def save_to_s3(data):
    # Function to save data to an S3 bucket
    bucket_name = 'peakevaluator'
    current_date = datetime.now().strftime("%Y-%m-%d")
    file_name = f'Logs_{current_date}.json'

    s3.put_object(Body=json.dumps(data, indent=2), Bucket=bucket_name, Key=file_name)

def lambda_handler(event, context):
        
        # Fetch random numbers
        random_numbers = get_random_numbers()

        # Evaluate PEAK values
        peak_result = evaluate_peak_values(random_numbers)

        # Save result to S3
        save_to_s3(peak_result)

        return {
            'statusCode': 200,
            'body': json.dumps('Process completed successfully!')
        }

