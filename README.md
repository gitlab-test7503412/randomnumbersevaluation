# Random Numbers Evaluation Using Lambda

## Overview

This is a Python script designed to run on AWS Lambda. It fetches random numbers from an external API, evaluates them based on certain criteria, and then saves the results in a formatted JSON file to an Amazon S3 bucket. Futher the output file from S3 bucket is visualised using the log aggregrator tool ELK Stack also integrated CloudWatch alarm to alert the end user using SNS about the performance of the Lambda function in case of any performace related issue.

### Architecture Visualization

[![Function Overview](./images/Flow-Chart.png)](./images/Flow-Chart.png)

## Setup Requirements

- Python 3.10
- Event Bridge
- AWS account with Lambda and S3 access
- Ec2 Instance
- ELK Stack
- Cloud Watch

## Prerequisites

- AWS account with necessary permissions to create and configure Lambda functions, EventBridge rules and Other Service.

# AWS Lambda Configuration

1. Open AWS Lambda Console: [AWS Lambda Console](https://console.aws.amazon.com/lambda/) and log in to your AWS account.
2. Create the Lambda Function
3. Open the `lambda_function.py` file.
4. Add you code and replace the S3 bucket name in the `save_to_s3` function with the required bucket name.

    ```python
    bucket_name = 'your_s3_bucket_name'
    ```
 [![Function Overview](./images/Lambda.png)](./images/Lambda.png)

5. Deploy the script as an AWS Lambda function.
6. Test if its working or not using Test option on the console.

## Functionality

- The script fetches random numbers from an API.
- It evaluates the numbers based on predefined criteria (PEAK values).
- The results are saved in a JSON file to a specified S3 bucket.
- The results are visualised using the ELK stack.

# EventBridge Trigger Setup

## Overview

This guide explains how to set up an EventBridge trigger for our Lambda function using the AWS Management Console. The trigger is scheduled to run the Lambda function at 9:00 AM IST daily (3:30 AM UTC).

### Steps

1. Open AWS Lambda Console: [AWS Lambda Console](https://console.aws.amazon.com/lambda/) and log in to your AWS account.
2. Select Lambda Function: Choose the Lambda function for which you want to create the trigger.
3. Add Trigger: In the Lambda function configuration, scroll down to the "Designer" section. Click on "Add trigger" and choose "EventBridge."
4. Configure EventBridge Rule: Click on "Configure triggers" and in the "Create a new rule" section, set the cron expression for 3:30 AM UTC: `cron(30 3 * * ? *)`.
5. Add Target: In the "Configure targets" section, choose "Lambda function" as the target and select your Lambda function from the dropdown.
6. Create Rule: Click on "Create function." Review the configuration details and click on "Add" to add the trigger to your Lambda function.

[![Function Overview](./images/function.png)](./images/function.png)

### Notes

- Ensure proper AWS IAM roles and permissions for Lambda and S3.
- In Lambda function click on `Configuration` then click on `Permissions` there by clcking on role name will be able to add futher policy to the current role.
- Check the Lambda logs for any errors during execution.

# S3 Bucket

Create the S3 bucket with the unique name and after creating the S3 bucket, add the following bucket policy to grant access to the bucket:

1. Navigate to the S3 bucket you created.
2. Click on the `Permissions` tab.
3. Under `Bucket Policy` click on `Edit`.
4. Add the following policy:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::your_s3_bucket_name",
                "arn:aws:s3:::your_s3_bucket_name/*"
            ]
        }
    ]
}
```
- The content of Output file which will be stored in S3 bucket will look like below.

[![Function Overview](./images/output.png)](./images/output.png)


# CloudWatch Alarm Setup

## Steps

1. **Create SNS Topic:**
   - Open the Amazon SNS Console: [Amazon SNS Console](https://console.aws.amazon.com/sns/).
   - Click on `Create topic`.
   - Enter a name and display name for your topic.
   - Click on `Create topic`.
   - Note the ARN (Amazon Resource Name) of the created topic.

2. **Subscribe to the SNS Topic:**
   - Select the newly created topic.
   - Click on `Create subscription`.
   - Choose the protocol as `Email`.
   - Enter your email address.
   - Click on `Create subscription`.
   - Confirm the subscription by clicking the link sent to your email.

3. **Create CloudWatch Alarm:**
   - Open the Amazon CloudWatch Console: [Amazon CloudWatch Console](https://console.aws.amazon.com/cloudwatch/).
   - In the left navigation pane, click on `Alarms` and then click on `Create alarm`.
   - Select `Lambda` as the service.
   - Choose the appropriate metric for your Lambda function.
   - Specify the conditions for your alarm.
   - In the `Actions` section, choose `State is ALARM`.
   - In the `Send notification to` dropdown, select `New SNS topic` and enter a name for the topic or ARN.
   - Click on `Create alarm`.
   - Create two alarms for `Duration` and `error` to monitor the performance.     

[![Function Overview](./images/OK.png)](./images/OK.png)

4. **Test the Alarm:**
   - Invoke your Lambda function in a way that triggers the conditions set in the CloudWatch alarm.
   - Wait for the alarm to enter the "ALARM" state.
   - You should receive a notification email indicating that the alarm has been triggered.

## IN-Alarm Status 

[![Function Overview](./images/in-alarm.png)](./images/in-alrm.png)

## Email Notification

[![Function Overview](./images/email.png)](./images/email.png)

## OK Notification

[![Function Overview](./images/ok-status.png)](./images/ok-status.png)

# Lambda Memory Utilization Analysis 

## Overview

Utilising CloudWatch Logs Insights to analyze the "REPORT" type logs generated by Lambda functions, on the basis of output we can make informed decisions about memory allocation, helping optimize resource usage and potentially reduce costs.

## Query

```sql
filter @type = "REPORT"
| stats max(@memorySize / 1000 / 1000) as provisionedMemoryMB,
  min(@maxMemoryUsed / 1000 / 1000) as smallestMemoryRequestMB,
  avg(@maxMemoryUsed / 1000 / 1000) as avgMemoryUsedMB,
  max(@maxMemoryUsed / 1000 / 1000) as maxMemoryUsedMB,
  provisionedMemoryMB - maxMemoryUsedMB as overProvisionedMB
```
## Output

[![Function Overview](./images/Memory.png)](./images/Memory.png)

## Filters

[![Function Overview](./images/values.png)](./images/values.png)


# PeakEvaluation ELK Stack Setup

## Overview

This guide outlines the steps to set up the PeakEvaluation project with the ELK (Elasticsearch, Logstash, Kibana) stack. We are using ELK stack for log aggregation, analysis, and visualization.

## Customization Note

For the Random Numbers Evaluation Using Lambda, the ELK stack will be utilized to analyze and visualize random number evaluations based on predefined criteria. The application fetches random numbers, evaluates them against PEAK values, and outputs the results in Kibana dashboard.

[![Function Overview](./images/Dashboard.png)](./images/Dashboard.png)

Below is the setup procedure for the ELK Stack Implementaion.

# Elasticsearch Setup

```bash
# Add Elasticsearch Repository Key
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
```
## Install apt-transport-https Package
```
sudo apt-get install apt-transport-https
```
## Add Elasticsearch Repository
```
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
```
## Install Elasticsearch
```
sudo apt-get update && sudo apt-get install elasticsearch
```
## Configure Elasticsearch
```
sudo bash -c 'cat <<EOL > /etc/elasticsearch/elasticsearch.yml
network.host: "localhost"
http.port: 9200
cluster.initial_master_nodes: ["<PrivateIP"]
EOL'
```
## Start Elasticsearch
```
sudo service elasticsearch start
```
## Verify Installation
```
sudo curl http://localhost:9200
```
# Logstash Setup

## Install Java
```
sudo apt-get install default-jre
```
## Install Logstash
```
sudo apt-get install logstash
```

## Configure Logstash
```
sudo vim /etc/logstash/conf.d/your_project.conf
```
## Add Logstash configuration specific to your project

```yaml
input {
    s3 {
        "access_key_id" => "AWS-Access-Key"
        "secret_access_key" => "AWS-Secret-Key"
        "region" => "region"
        "bucket" => "s3-Bucket"
        "prefix" => "Filename"
        "interval" => "10"
        "codec" => "json"
        "additional_settings" => {
            "force_path_style" => true
            "follow_redirects" => false
            }
    }
}
            
output {
    elasticsearch {
        hosts => ["http://localhost:9200"]
        index => "logs-%{+YYYY.MM.dd}"
    }
}
```
## Start Logstash
```
sudo service logstash start
```
## To make sure the data is being indexed, use:
```
sudo curl -XGET 'localhost:9200/_cat/indices?v&pretty'
```
**You should see your new Logstash index created**

## Check Logstash logs
```
sudo cat /var/log/logstash/logstash-plain.log
```
# Kibana Setup

## Install Kibana
```
sudo apt-get install kibana
```
## Configure Kibana
```
sudo bash -c 'cat <<EOL > /etc/kibana/kibana.yml
server.port: 5601
server.host: "localhost"
elasticsearch.hosts: ["http://localhost:9200"]
EOL'
```
## Start Kibana
```
sudo service kibana start
```
# Securing Kibana with Basic Authentication

## Overview

This guide outlines the steps to enhance the security of your Kibana dashboard by implementing basic authentication using an `htpasswd` file. Basic authentication adds an extra layer of security, ensuring that only authorized users with valid credentials can access Kibana.

## Why Use Basic Authentication?

When Kibana is exposed to the internet or any external network, implementing basic authentication becomes crucial. It helps protect sensitive information and prevents unauthorized access. This security measure is especially important for preventing potential security threats and unauthorized usage.

## Prerequisites

- Ubuntu Server
- Nginx installed
- Access to the terminal with sudo privileges

## Step-by-Step Guide

### Step 1: Install Nginx Utilities

```bash
sudo apt install nginx -y
```

### Step 1: Install Apache2 Utilities

```bash
sudo apt install -y apache2-utils

sudo htpasswd -c /etc/nginx/htpasswd.users admin
```

You will be prompted to enter a password for the user. Make note of the username (admin) and the chosen password.

### Step 2: Configure Nginx

Edit the Nginx configuration file:

```bash
sudo vim /etc/nginx/sites-available/default
```

Add the following block inside the server block:

```bash
server {
    listen 80;
    server_name YOUR_SERVER_IP_OR_DOMAIN;

    auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

    location / {
        proxy_pass http://localhost:5601;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```
### Step 3: Restart Nginx

```bash
sudo systemctl restart nginx
```
### Step 4: Access Kibana

Visit http://YOUR_SERVER_IP_OR_DOMAIN in your web browser.

You will be prompted to enter the username (admin) and the password you set in step 2.

[![Function Overview](./images/Auth.png)](./images/Auth.png)

This completes the setup, adding basic authentication to your Kibana dashboard for improved security.


# Conclusion

In conclusion, the Random Numbers Evaluation Using Lambda leverages AWS services and the ELK (Elasticsearch, Logstash, Kibana) stack to efficiently evaluate and visualize random number data. The key components include:

**Random Numbers Evaluation Lambda**: A Python script running on AWS Lambda fetches random numbers, evaluates them based on defined criteria, and stores the results in an S3 bucket.

**EventBridge Trigger Setup**: An EventBridge rule is configured to trigger the Lambda function daily at 9:00 AM IST, ensuring regular evaluations.

**CloudWatch Alarm Setup**: CloudWatch alarms are implemented to monitor the Lambda function's performance. Alerts are sent via SNS in case of issues, providing timely notifications to stakeholders.

**ELK Stack Setup**: The ELK stack is employed for log aggregation, analysis, and visualization. Elasticsearch stores the evaluated data, Logstash processes it, and Kibana provides a user-friendly interface for visualizing the results.

## Author

**Dushyant Rahangdale**
